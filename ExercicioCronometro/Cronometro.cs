﻿

namespace ExercicioCronometro
{

    using System;


    public class Cronometro
    {

        #region atributos

        private DateTime _start;

        private DateTime _stop;

        private bool _isrunning;// indica se o cronometro está ligado ou desligado

        // como no lado de fora da classe não vamos utilizar estes valores, não precisamos de definir propriedades
        // Não vamos utilizar também os construtores

        #endregion

        public void StartClock()

        {
            // se já estiver ligado, vai dizer que já está ligado. Se estiver desligado, dá o tempo NOW e liga o cronómetro

            if (_isrunning)
            {

                throw new InvalidOperationException(" O cronómetro já está ligado");
                // interrompe o programa e manda menssagem;
                // está apenas por segurança pois não vai ser necessário uma vez que apenas vamos ter um botão
                // este if não precisa de else porque se entra sai logo.                          

            }

            _start = DateTime.Now;

            _isrunning = true;

        }




        public void StopClock()
        {
            // quando desligarmos o cronometro e se ele estiver ligado vai buscar o tempo NOW e desliga o cronometro. Se estiver já desligado dá a mensagem.


            if (!_isrunning)
            {

                throw new InvalidOperationException(" O cronómetro já está desligado");
                // interrompe o programa e manda menssagem;
                // está apenas por segurança pois não vai ser necessário uma vez que apenas vamos ter um botão
                // este if não precisa de else porque se entra sai logo.                          

            }

            _stop = DateTime.Now;

            _isrunning = false;

        }



        public TimeSpan GetTimeSpan()
        {
            return _stop - _start; // tempo que o cronometro estve ligado

           
        }

        public bool ClockState()
        {
            return _isrunning; // indica se está ligado ou não
        }



        public DateTime StartTime()
        {
            return _start; // guarda o tempo start e envia para fora;
        }
    }
}
